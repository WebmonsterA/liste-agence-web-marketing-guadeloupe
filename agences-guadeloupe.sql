CREATE TABLE agences_guadeloupe(
   NOM     VARCHAR(23) NOT NULL PRIMARY KEY
  ,ADDRESS VARCHAR(45)
  ,ADRESS2 VARCHAR(26)
  ,CPO     INTEGER 
  ,CITY    VARCHAR(14)
  ,PHONE1  VARCHAR(14) NOT NULL
  ,PHONE2  VARCHAR(14)
  ,EMAIL   VARCHAR(32)
  ,URL     VARCHAR(38)
  ,TYPE    VARCHAR(3) NOT NULL
);
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Altitude','530 rue de la Chapelle, Immeuble La Caravelle','Zone Industrielle de Jarry',97122,'Baie-Mahault','05 90 60 47 68',NULL,'contact@altitudegp.com','https://altitudegp.com/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Antilles Design','10 lot beau Vallon',NULL,97129,'Lamentin','06 58 14 70 62',NULL,'contact.antillesdesign@gmail.com','https://www.agence-web-guadeloupe.com/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Beeliz','16b les jardins de houelbourg','Zone Industrielle de Jarry',97122,'Baie-Mahault','05 90 69 85 69',NULL,'info@beeliz.com','https://www.beeliz.com/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('CUBTEC','83 Le Clos Alicéa',NULL,97122,'Baie-Mahault','06 90 48 07 83',NULL,NULL,'https://cubtec.com/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Digital Design Création','chemin Neuf',NULL,97114,'Trois-Rivières','06 60 50 62 16',NULL,'contact@ddcreation.fr','https://www.digitaldesigncreation.fr/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Digitallis','Immeuble La City','Zone Industrielle de Jarry',97122,'Baie-Mahault','05 90 26 01 02',NULL,'fabrice.pech@digitallis.fr','https://digitallis.fr/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Digitom','938 bis, rue Henri Becquerel','Zone Industrielle de Jarry',97122,'Baie-Mahault','05 90 99 99 30',NULL,NULL,'https://www.digitom.fr/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('GM SEO','6 rue du Dr Antonio Ferly',NULL,97180,'Sainte-Anne','06 50 09 34 01',NULL,'info@gmseo.fr','https://www.gmseo.fr/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Gwada Web Design','145 rue Campeche',NULL,97125,'Bouillante','06 90 19 88 60','06 90 19 88 60','contact@gwadawebdesign.com','https://www.gwadawebdesign.com/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Karukera Creativ','Faubourg Victor Hugo',NULL,97110,'Pointe-A-Pitre','06 95 00 41 33','05 90 02 12 30','contact@karukeracreativ.com','https://karukeracreativ.com/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Karuweb','Résidence Raisins Clairs','H101 Section SEZE',97118,'Saint-François','06 90 86 13 55',NULL,'contact@karuweb.fr','https://www.karuweb.fr/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Katchak Digital Agency',NULL,NULL,NULL,NULL,'05 90 69 69 65','06 90 34 63 66','contact@katchak-agency.fr','https://katchak-agency.fr/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Kawuk','Route de la Pointe gros boeuf',NULL,97118,'Saint-François','05 90 24 39 93',NULL,'contact@kawuk.com','https://kawuk.com/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('LyannajWeb','Grand camp',NULL,97139,'Abymes (Les)','06 90 83 54 19',NULL,'hello@lyannajweb.fr','https://lyannajweb.fr/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('MJ Metrix','rue Henri Becquerel',NULL,97122,'Baie-Mahault','06 90 43 26 09',NULL,'direction@mjmetrix.com','https://mjmetrix.com/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Studio du Web','52 res Belle Allée',NULL,97118,'Saint-François','06 90 24 11 64','06 90 16 77 79','contact@studioduweb.biz','https://studioduweb.biz/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Web Expansion Agency','15 rue Abel Libany','Quartier Assainissement',97139,'Abymes (Les)','06 90 67 76 31',NULL,'contact@webexpansion.fr',NULL,'Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Web2Web','Route des télécommunications',NULL,97122,'Baie-Mahault','06 90 84 95 98',NULL,'direction@web2web.fr','https://web2web.fr/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Yékréa','24 Rue Ferdinand Forest',NULL,97122,'Baie-Mahault','06 90 85 78 61',NULL,'contact@yekrea.com','https://www.yekrea.com/','Web');
INSERT INTO agences_guadeloupe(NOM,ADDRESS,ADRESS2,CPO,CITY,PHONE1,PHONE2,EMAIL,URL,TYPE) VALUES ('Yo Médya','Rue Nassau',NULL,97110,'Pointe-A-Pitre','06 90 50 02 06','09 72 33 73 19','contact@yo-medya.fr','https://www.yo-medya.fr/','Web');
